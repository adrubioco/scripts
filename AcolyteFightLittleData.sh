#!/bin/sh

# This script outputs like this:
# {"timestamp":"2020-02-05T11:12+01:00",
#  "regions": [
#      "Europe": {"region":"eu","host":"eu-9whm","numPlayers":4},
#      "Murica": {"region":"us","host":"us-jgd5","numPlayers":0},
#      "Asia": {"region":"sg","host":"sg-6fjm","numPlayers":0}
# ]}

euStatusURL="https://eu.acolytefight.io/status"
usStatusURL="https://us.acolytefight.io/status"
sgStatusURL="https://sg.acolytefight.io/status"
eup="/tmp/euData.pipe"
usp="/tmp/usData.pipe"
sgp="/tmp/sgData.pipe"
# The time standard of the WWW: https://en.wikipedia.org/wiki/ISO_8601
currentDate="$(date --iso-8601=minutes)"

# Fetch each JSON in parallel,
# take the results from their respective pipes
# and close the pipes
mkfifo $eup ; mkfifo $usp ; mkfifo $sgp
curl -s "$euStatusURL" >"$eup" &
curl -s "$usStatusURL" >"$usp" &
curl -s "$sgStatusURL" >"$sgp" &
euData="$(cat <"$eup")"; rm "$eup"
usData="$(cat <"$usp")"; rm "$usp"
sgData="$(cat <"$sgp")"; rm "$sgp"

# Format a JSON containing the timestamp
# and the data of the 3 regions
printf\
    "{\"timestamp\":\"%s\",\"regions\": [%s,%s,%s]}\n"\
    "$currentDate" \
    "\"Europe\": $euData"\
    "\"Murica\": $usData"\
    "\"Asia\": $sgData"
