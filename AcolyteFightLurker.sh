#!/bin/sh

euStatusURL="https://eu.acolytefight.io/status"
usStatusURL="https://us.acolytefight.io/status"
sgStatusURL="https://sg.acolytefight.io/status"

# Download the status JSON for each region
# and extract the player count field's value
fetch() {
    curl -s "$1" |\
        sed "s;.*\"numPlayers\":\([0-9][0-9]*\)};\1;"
}

sleep_time=5m
# Every 5 minutes, check for player count
while sleep $sleep_time; do
    euPlayerCount="$(fetch "$euStatusURL")"
    usPlayerCount="$(fetch "$usStatusURL")"
    sgPlayerCount="$(fetch "$sgStatusURL")"

    # You need a notification daemon running for this command to work
    notify-send "Acolyte fight player count:" \
        "$(printf "%s\n" \
        "Europe: $euPlayerCount"\
        "Murica: $usPlayerCount"\
        "Asia: $sgPlayerCount")"\
        --icon=dialog-information
done
