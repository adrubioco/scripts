# Scripts

Some shell and other language scripts done for fun.

What do they do?
================

* **AcolyteFightLittleData.sh:**
    Retrieves data from the AcolyteFight game API, adds a timestamp, and outputs to stdout
    Format:
    ```JSON
    {"timestamp":"2020-02-05T11:12+01:00",
        "regions": [
            "Europe": {"region":"eu","host":"eu-9whm","numPlayers":4},
            "Murica": {"region":"us","host":"us-jgd5","numPlayers":0},
            "Asia": {"region":"sg","host":"sg-6fjm","numPlayers":0}
    ]}
    ```
    I'm using it to collect data about the number of players.

* **AcolyteFightLurker.sh**
    Retrieves the number of players on every region from the AcolyteFight game API, and sends a desktop notification.
    You need a notification daemon for this script to work, but every major Desktop Environment includes one. I recommend [Dunst](https://dunst-project.org/).
